#!/bin/bash

make clean
clang-format -i *.c *.h
clang-format -n *.c *.h