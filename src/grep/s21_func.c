#include "s21_grep.h"

void print_match(Toptions *options, char *filename, char *source, int num) {
  /* Разбор работы regcomp и regexec https://russianblogs.com/article/7464409418
   */
  regex_t regex;
  regmatch_t pmatch[1000];
  int regcomp_status =
      regcomp(&regex, options->template,
              options->i ? REG_ICASE | REG_EXTENDED : REG_EXTENDED);
  if (!(regcomp_status)) {
    int regex_status = regexec(&regex, source, 0, NULL, 0);
    // printf("[regexec: %d] => ", regex_status);
    // printf("OT: %s \n", options->template);
    if (regex_status == options->v) {
      options->countMatches += 1;
      if (!options->c && !options->l) {
        if (options->countFiles > 1 && !(options->h) && !(options->o)) {
          prints_file(filename);
          printf(":");
        }
        if (options->n && !(options->o)) {
          prints_snum(num);
          printf(":");
        }
        char *ps = source;
        while (regexec(&regex, ps, 1, pmatch, 0) == 0) {
          if (options->countFiles > 1 && !(options->h) && options->o) {
            prints_file(filename);
            printf(":");
          }
          if (options->n && options->o) {
            prints_snum(num);
            printf(":");
          }
          int pos = pmatch[0].rm_so;  // начало совпадения
          // printf("[POS:%d]", pos);
          if (pos >= 0 && !options->o) substr(ps, pos, 0);
          if (pos == 0 && options->o) substr(ps, pos, 0);
          ps += pos;
          substr(ps, pmatch[0].rm_eo - pos, 1);
          if (options->o) printf("\n");
          ps -= pos;
          ps += pmatch[0].rm_eo;
        }
        if (!options->o) {
          printf("%s", ps);  // Допечатка строки
          print_lastchar(ps);
        }
      }
    }
  }
  regfree(&regex);
}

void grepOutput(Toptions options, char *filename) {
  options.countMatches = 0;
  FILE *f = fopen(filename, "rt");
  if (f != NULL) {
    char line[500] = {0};
    int num = 0;
    while (fgets(line, sizeof(line), f)) {
      num++;
      // printf("OT: %s\n", template);
      print_match(&options, filename, line, num);
    }
    if (options.c && !options.l /*(Linux version)*/) {
      if (options.countFiles > 1 && !(options.h)) {
        prints_file(filename);
        printf(":");
      }
      printf("%d\n", options.countMatches);
    }
    if (options.countMatches > 0) {
      if (options.l) {
        prints_file(filename);
        printf("\n");
      }
    }
    fclose(f);
  } else {
    if (options.countFiles > 1 && options.l)
      fprintf(stderr, FILE_ERROR, filename);
    // exit(EXIT_SUCCESS);
  }
}

void construct_f_template(Toptions *options) {
  FILE *f = fopen(optarg, "r");
  char line[BUFSIZE];
  if (f != NULL) {
    while (fgets(line, BUFSIZE, f) != NULL) {
      if (line[strlen(line) - 1] == '\n' && strlen(line) - 1 != '\0') {
        line[strlen(line) - 1] = '\0';
      }
      strcat(options->template, line);
      strcat(options->template, "|");
    }
    options->template[strlen(options->template) - 1] = '\0';
  } else {
    fprintf(stderr, FILE_ERROR, optarg);
    exit(EXIT_SUCCESS);
  }
  fclose(f);
}

void parseOptions(Toptions *options, int argc, char **argv) {
  /* GETOPT:
   * https://translated.turbopages.org/proxy_u/en-ru.ru.76b4ee9c-655f69a4-8fed8441-74722d776562/https/www.geeksforgeeks.org/getopt-function-in-c-to-parse-command-line-arguments/
   */
  for (int sym = 0; (sym = getopt(argc, argv, "e:ivclnhsf:o")) != (-1);) {
    switch (sym) {
      case 'f':
        options->f = 1;
        strcat(options->f_filename, optarg);
        construct_f_template(options);
        break;
      case 'e':
        options->e = 1;
        strcat(options->earg, optarg);
        strcat(options->earg, "|");
        break;
      case 'l':
        options->l = 1;
        break;
      case 'c':
        options->c = 1;
        break;
      case 'o':
        options->o = 1;
        break;
      case 'i':
        options->i = 1;
        break;
      case 'v':
        options->v = 1;
        break;
      case 's':
        options->s = 1;
        break;
      case 'n':
        options->n = 1;
        break;
      case 'h':
        options->h = 1;
        break;
      default:
        fprintf(stderr, EMPTY_OPTIONS);
        exit(EXIT_SUCCESS);
    }
  }
  if (options->e) {
    if (options->earg[strlen(options->earg) - 1] == '|')
      options->earg[strlen(options->earg) - 1] = '\0';
    if (options->f) {
      strcat(options->template, "|");
    }
    strcat(options->template, options->earg);
  }
}

/* ВСПОМОГАТЕЛЬНЫЕ ФУНКЦИИ */

void argtest(Toptions options) {
  /* ТЕСТ АРГУМЕНТОВ */
  printf("Флаги и аргументы:\n");
  printf("-e=%d earg:%s\n", options.e, options.earg);
  printf("-f=%d pattern_file:%s\n", options.f, options.f_filename);
  printf("-i=%d -v=%d -c=%d -l=%d -n=%d -h=%d -s=%d -o=%d\n", options.i,
         options.v, options.c, options.l, options.n, options.h, options.s,
         options.o);
  printf("TEMPLATE: %s\n", options.template);
  printf("FILES COUNT: %d\n", options.countFiles);
  printf("------------------------------\n");
  /* ТЕСТ АРГУМЕНТОВ */
}

void print_lastchar(char *source) {
  char lastchar = source[strlen(source) - 1];
  if (lastchar != '\n') printf("\n");
}

void substr(char *source, int length, int ifcolor) {
  if (COLORS_ON && ifcolor) printf(RED);
  for (int i = 0; i < length; i++) printf("%c", source[i]);
  if (COLORS_ON && ifcolor) printf(COLOR_END);
}

void prints_file(char *filename) {
  if (COLORS_ON) printf(MAGENTA);
  printf("%s", filename);
  if (COLORS_ON) printf(COLOR_END);
}

void prints_snum(int num) {
  if (COLORS_ON) printf(GREEN);
  printf("%d", num);
  if (COLORS_ON) printf(COLOR_END);
}