#!/bin/bash
SUCCESS=0
FAIL=0
COUNTER=0
DIFF=""

s21_command=(
    "s21_grep"
    )

sys_command=(
    "grep"
    )

flags=(
    "i"
    "v"
    "c"
    "l"
    "n"
    "h"
    "o"
    "s"
)

tests=(
"for test_files/test_1_grep.txt test_files/test_2_grep.txt test_files/test_3_grep.txt FLAGS"
"for test_files/test_1_grep.txt FLAGS"
"-e for -e int test_files/test_1_grep.txt test_files/test_2_grep.txt test_files/test_3_grep.txt FLAGS"
"-e for -e int test_files/test_1_grep.txt FLAGS"
"-e print -e int test_files/test_1_grep.txt FLAGS -f test_files/test_ptrn_grep.txt"
"-e while -e for test_files/test_1_grep.txt test_files/test_2_grep.txt test_files/test_3_grep.txt FLAGS -f test_files/test_ptrn_grep.txt"
)

manual=(
"-n for test_files/test_1_grep.txt test_files/test_2_grep.txt"
"-n for test_files/test_1_grep.txt"
"-n -e ^\} test_files/test_1_grep.txt"
"-c -e \/ test_files/test_1_grep.txt"
"-ce ^int test_files/test_1_grep.txt test_files/test_2_grep.txt"
"-e ^int test_files/test_1_grep.txt"
"-nivh = test_files/test_1_grep.txt test_files/test_2_grep.txt"
"-e"
"-ie INT test_files/test_5_grep.txt"
"-echar est_files/test_1_grep.txt test_files/test_2_grep.txt"
"-ne = -e out test_files/test_5_grep.txt"
"-iv int test_files/test_5_grep.txt"
"-in int test_files/test_5_grep.txt"
"-c -l aboba test_files/test_1_grep.txt test_files/test_5_grep.txt"
"-v test_files/test_1_grep.txt -e ank"
"-noe ')' test_files/test_5_grep.txt"
"-l for test_files/test_1_grep.txt test_files/test_2_grep.txt"
"-o -e int test_files/test_4_grep.txt"
"-e = -e out test_files/test_5_grep.txt"
"-noe ing -e as -e the -e not -e is test_files/test_6_grep.txt #в этом тесте grep мака выдает ПОЛНУЮ ХРЕНЬ! Никакой логики"
"-e ing -e as -e the -e not -e is test_files/test_6_grep.txt"
"-l for no_file.txt test_files/test_2_grep.txt"
"-e int -si no_file.txt s21_grep.c no_file2.txt s21_grep.h"
"-si s21_grep.c -f no_pattern.txt"
"-f test_files/test_ptrn_grep.txt test_files/test_5_grep.txt"
)

run_test() {
    param=$(echo "$@" | sed "s/FLAGS/$var/")
     #для MAC
     #unsetopt NOMATCH
    ../"${s21_command[@]}" $param > logs/"${s21_command[@]}".log 2>&1
    "${sys_command[@]}" $param > logs/"${sys_command[@]}".log 2>&1
    #для Ubuntu 
    #../"${s21_command[@]}" $param &> logs/"${s21_command[@]}".log
    #"${sys_command[@]}" $param &> logs/"${sys_command[@]}".log
    DIFF="$(diff -s logs/"${s21_command[@]}".log logs/"${sys_command[@]}".log)"
    let "COUNTER++"
    if [ "$DIFF" == "Files "logs/${s21_command[@]}".log and "logs/${sys_command[@]}".log are identical" ]
    then
        let "SUCCESS++"
        echo -e "\033[92m$COUNTER - Success\033[0m $param"
    else
        let "FAIL++"
        echo -e "\033[91m$COUNTER - Fail\033[0m $param"
    fi
    rm -f logs/"${s21_command[@]}".log logs/"${sys_command[@]}".log
}

echo -e "\033[91mВНИМАНИЕ! Системный grep НА маках КРИВУЩЩИЙ. Некорректная обработка совместных флагов -c -l\033[0m"
echo -e "\033[96m##################################\033[0m"
echo -e "\033[96mMANUAL TESTS (Извращался, как мог)\033[0m"
echo -e "\033[96m##################################\033[0m"
printf "\n"

for i in "${manual[@]}"
do
    var="-"
    run_test "$i"
done

printf "\n"
echo -e "\033[96m=======================\033[0m"
echo -e "\033[96mAUTOTEST 1 PARAMETER\033[0m"
echo -e "\033[96m=======================\033[0m"
printf "\n"

for var1 in "${flags[@]}"
do
    for i in "${tests[@]}"
    do
        var="-$var1"
        run_test "$i"
    done
done
printf "\n"
echo -e "\033[96m=======================\033[0m"
echo -e "\033[96mAUTOTEST 2 PARAMETERS\033[0m"
echo -e "\033[96m=======================\033[0m"
printf "\n"

for var1 in "${flags[@]}"
do
    for var2 in "${flags[@]}"
    do
        if [ $var1 != $var2 ]
        then
            for i in "${tests[@]}"
            do
                var="-$var1 -$var2"
                run_test "$i"
            done
        fi
    done
done
printf "\n"

echo -e "\033[91mFAIL: $FAIL\033[0m"
echo -e "\033[92mFAIL: $SUCCESS\033[0m"
echo -e "\033[96mALL: $COUNTER\033[0m"
printf "\n"
##############################
