#include "s21_grep.h"

int main(int argc, char *argv[]) {
  Toptions options = {0};
  parseOptions(&options, argc, argv);  //парсим опции, строим шаблоны

  if (!options.e && !options.f) {
    strcat(options.template,
           argv[optind]);  // заполняем шаблон из опций, если не заданы -e и -f
    optind += 1;           // переключаем оптинд
  }
  options.countFiles = argc - optind;
  // argtest(options); // тестирование правильности считывания аргументов*/

  for (int i = optind; i < argc; i++)  // прогон файлов
    grepOutput(options, argv[i]);
  return 0;
}